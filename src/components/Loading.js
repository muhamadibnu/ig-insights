import React from 'react';

export default function Loading() {
	return (
		<>
			<div class='preloader'>
				<div class='lds-ripple'>
					<div class='lds-pos'></div>
					<div class='lds-pos'></div>
				</div>
			</div>
		</>
	);
}
