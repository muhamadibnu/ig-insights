let Url;
require('dotenv').config();
switch (process.env.NODE_ENV) {
	case 'development':
		Url = process.env.REACT_APP_BASE_URL_DEV;
		break;
	case 'production':
		Url = process.env.REACT_APP_BASE_URL_PROD;
		break;
	case 'test':
		Url = process.env.REACT_APP_BASE_URL_TEST;
		break;
	default:
		Url = process.env.REACT_APP_BASE_URL_DEV;
		break;
}

export const apiUrl = Url;
