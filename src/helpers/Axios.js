const axios = require('axios');
const { apiUrl } = require('../config/baseUrl');

const defaultHeaders = {
	// Authorization: 'Basic ZGVtbzpkZW1v',
	'Content-Type': 'application/json',
};

const multipartHeaders = {
	Authorization: 'Basic ZGVtbzpkZW1v',
	Accept: 'application/json',
	'Content-Type': 'multipart/form-data',
};

axios.interceptors.request.use(
	(config) => {
		const accessToken = localStorage.getItem('accessToken');
		if (accessToken) {
			config.headers['x-auth-token'] = accessToken;
		}
		return config;
	},
	(error) => {
		Promise.reject(error);
	}
);

axios.interceptors.response.use(
	(response) => {
		return response;
	},
	function (error) {
		const originalRequest = error.config;
		let refreshToken = localStorage.getItem('refreshToken');
		if (refreshToken && error.response.status === 401 && !originalRequest._retry) {
			originalRequest._retry = true;
			return axios.post(`${apiUrl}/auth/refresh_token`, { refreshToken: refreshToken }).then((res) => {
				if (res.status === 200) {
					const { accessToken, refreshToken } = res.data;
					localStorage.setItem('accessToken', accessToken);
					localStorage.setItem('refreshToken', refreshToken);
					return axios(originalRequest);
				}
			});
		}
		return Promise.reject(error);
	}
);

const Axios = {
	Get: ({ url, params, data }) => {
		return new Promise((resolve, reject) => {
			axios
				.request({
					method: 'GET',
					url: `${url}/${params}`,
					headers: {
						...defaultHeaders,
					},
					data,
				})
				.then((response) => {
					console.log('response: ', response);
					resolve(response.data);
				})
				.catch((error) => {
					if (axios.isCancel(error)) {
						console.log('Request canceled', error);
					} else {
						reject(error);
					}
				});
		});
	},

	Post: ({ url, data, cancelToken = '' }) => {
		console.log('url: ', url);
		return new Promise((resolve, reject) => {
			axios
				.request({
					method: 'POST',
					url: url,
					data: data,
					headers: {
						...defaultHeaders,
					},
					cancelToken: cancelToken,
				})
				.then((response) => {
					resolve(response.data);
				})
				.catch((error) => {
					console.log('axios error', error);
					if (axios.isCancel(error)) {
						console.log('Request canceled', error);
					} else {
						reject(error);
					}
				});
		});
	},

	PostFormData: ({ url, data, cancelToken }) => {
		return new Promise((resolve, reject) => {
			axios
				.post(url, data, {
					headers: {
						...defaultHeaders,
					},
					cancelToken: cancelToken,
				})
				.then((response) => {
					resolve(response.data);
				})
				.catch((error) => {
					if (axios.isCancel(error)) {
						// console.log('Request canceled', error);
					} else {
						reject(error);
					}
				});
		});
	},

	PostMultipart: ({ url, data, cancelToken }) => {
		return new Promise((resolve, reject) => {
			axios
				.post(url, data, {
					headers: {
						...multipartHeaders,
					},
					cancelToken: cancelToken,
				})
				.then((response) => {
					resolve(response.data);
				})
				.catch((error) => {
					if (axios.isCancel(error)) {
						// console.log('Request canceled', error);
					} else {
						reject(error);
					}
				});
		});
	},
};

export default Axios;
