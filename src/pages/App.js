import React, { useEffect, useState } from 'react';
import { Provider, useSelector } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { routes } from '../routes';
import Layout from './layout';
import { Error404 } from './Error';
import configureStore, { history } from '../redux';
import { ConnectedRouter } from 'connected-react-router';
const store = configureStore();

function App() {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<Router>
					<div className='App h-100'>
						<Route exact path='/'>
							<Redirect to='/home' />
						</Route>
						<Switch>
							{routes.map((route, i) =>
								route.isPublic ? (
									<PublicRoute key={i} {...route} />
								) : (
									<PrivateRoute key={i} {...route} />
								)
							)}
						</Switch>
						<Route component={Error404} />
					</div>
				</Router>
			</ConnectedRouter>
		</Provider>
	);

	function PrivateRoute(route) {
		const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken') || false);
		const { user } = useSelector((state) => state.AuthReducer);
		useEffect(() => {
			if (localStorage.getItem('accessToken') !== undefined) {
				setAccessToken(localStorage.getItem('accessToken'));
			}
			return () => {};
		}, [user]);
		return (
			<Route
				path={route.path}
				render={(props) =>
					accessToken ? (
						<Layout>
							<route.component {...props} routes={route.routes} />
						</Layout>
					) : (
						<Redirect
							to={{
								pathname: '/login',
								state: {
									from: props.location,
								},
							}}
						/>
					)
				}
			/>
		);
	}

	function PublicRoute(route) {
		const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken') || false);
		const { user } = useSelector((state) => state.AuthReducer);
		useEffect(() => {
			if (localStorage.getItem('accessToken')) {
				setAccessToken(localStorage.getItem('accessToken'));
			}
			return () => {};
		}, [user]);
		return (
			<Route
				path={route.path}
				render={(props) =>
					accessToken ? (
						<Redirect
							to={{
								pathname: '/',
							}}
						/>
					) : (
						<Route
							path={route.path}
							render={(props) => <route.component {...props} routes={route.routes} />}
						/>
					)
				}
			/>
		);
	}
}

export default App;
