import React, { useState } from 'react';
// import { useForm } from 'react-hook-form';
import FacebookLogin from 'react-facebook-login';

//assets
import Logo from '../../assets/images/logo.png';

// redux action
// import { getLogin } from '../../redux/actions/authAction';
import { useHistory } from 'react-router-dom';

export const Login = () => {
	const history = useHistory();

	const [isLoading, setIsLoading] = useState(false);

	return (
		<>
			<div
				id='main-wrapper'
				data-theme='dark'
				data-layout='vertical'
				data-navbarbg='skin1'
				data-boxed-layout='full'>
				<div className='app-container' data-navbarbg='skin1'></div>
				<div className='container page-breadcrump min-vh-100'>
					<div className='row'>
						<div className='col col-md-6'>
							<div className='h-100 d-flex align-items-center'>
								<div className='p-3'>
									<img src={Logo} width='200' alt='logo_clozette' />
									<h3 className='mt-4 text-black-50'>Welcome to</h3>
									<h1 className='text-black'>Clozette Insights Indonesia</h1>
								</div>
							</div>
						</div>
						<div className='col col-md-6'>
							<div className='min-vh-100 d-flex align-items-center'>
								<div className='w-100'>
									<div className='card'>
										<div className='card-header'>
											<h2>Log In</h2>
										</div>

										<div className='card-body text-center'>
											{isLoading ? (
												<div class='spinner-border' role='status'>
													<span class='visually-hidden'>Loading...</span>
												</div>
											) : (
												<div>
													<h5 className='p-2'>Please login with facebook account</h5>
													<FacebookLogin
														appId='149964243732146'
														autoLoad={false}
														size='small'
														fields='name,email,picture'
														scope='public_profile,email,pages_show_list,instagram_basic,ads_management,pages_read_engagement'
														callback={(e) => {
															localStorage.setItem('accessToken', e.accessToken);
															let condition = true;
															while (condition) {
																setIsLoading(true);
																if (e.accessToken) {
																	history.push('/');
																	condition = false;
																	setIsLoading(false);
																}
															}
														}}
													/>
												</div>
											)}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
