import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FacebookLogin from 'react-facebook-login';
import Swal from 'sweetalert2';
import { Line } from 'react-chartjs-2';
import {
	fetchMedia,
	fetchUserInsights28Days,
	getInstagramPage,
	getUserInsightsByDay,
	patchDashboardSelected,
} from '../redux/actions/userAction';

export const Home = () => {
	const dispatch = useDispatch();
	const { userSelected, userInsights, media, impressionsAndReach, urlPage, pagePermission, loading } = useSelector(
		(state) => state.AuthReducer
	);

	const [fbsToken, setFbsToken] = useState();

	useEffect(() => {
		if (pagePermission.length === 0 && fbsToken) {
			// dispatch(getInstagramPage(fbsToken));
		}
	}, [pagePermission, fbsToken]);

	const onSelect = (e) => {
		if (pagePermission[e.target.value].instagram_business_id) {
			dispatch(patchDashboardSelected(pagePermission[e.target.value]));
			dispatch(fetchMedia(pagePermission[e.target.value].instagram_business_id, urlPage));
			dispatch(fetchUserInsights28Days());
			dispatch(getUserInsightsByDay(pagePermission[e.target.value].instagram_business_id));
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Dont have instagram bussines account',
				html: '1. Openg Link <a href="https://www.facebook.com/pages/create" target="_blank">Facebook Pages Create</a> <br/> 2. Fill page name and category field And then click <b>Save</b> button <br/> 3. Go to the sidebar and scroll down to <b>Settings</b> tab and click it <br/> 4. Scroll down to <b>Instagram</b> tab and click connect account make sure your Instagram is isntagram business account <br/> 5. Logout dashboard and Login again',
			});
		}
	};

	const getMoreMedia = (e) => {
		e.preventDefault();
		dispatch(fetchMedia(userSelected.instagram_business_id, urlPage));
	};

	const data = {
		// labels: [18, 19, 20],
		labels: impressionsAndReach[0]?.values?.map((insights) => {
			return new Date(insights.end_time).toISOString().slice(0, 10);
		}),
		datasets: [
			{
				label: `# of ${impressionsAndReach.length ? impressionsAndReach[0]?.title : ''}`,
				// data: [0, 2, 4],
				data: impressionsAndReach[0]?.values?.map((insights) => {
					return insights.value;
				}),
				fill: false,
				backgroundColor: '#3751FF',
				borderColor: '#3751FF',
			},
		],
	};

	const options = {
		scales: {
			yAxes: [
				{
					ticks: {
						beginAtZero: true,
					},
				},
			],
		},
	};

	return (
		<>
			<div className='page-breadcrumb'>
				<div className='row'>
					<div className='col-7 align-self-center'>
						<h1 className='page-title text-truncate text-dark font-weight-medium mb-1'>
							Welcome to {userSelected.name || ''} Insights!
						</h1>
						<div className='d-flex align-items-center'>
							<nav aria-label='breadcrumb'>
								<ol className='breadcrumb m-0 p-0'>
									<li className='breadcrumb-item'>
										<a href='/' className='text-muted'>
											Dashboard
										</a>
									</li>
								</ol>
							</nav>
						</div>
					</div>
					<div className='col-5 align-self-center'>
						<div className='customize-input float-right'>
							{pagePermission.length > 0 ? (
								<div>
									<h6>Select Instagram Business Account </h6>
									<select className='custom-select form-control' onChange={onSelect} defaultValue=''>
										<option value='' disabled>
											== Select Instagram Acc ==
										</option>
										{pagePermission.length > 0 &&
											pagePermission.map((el, idx) => (
												<option value={idx} key={idx}>
													{el.name}
												</option>
											))}
									</select>
								</div>
							) : (
								<div className='position-static'>
									{loading && (
										<div className='text-center bg-light-50 position-absolute ml-5'>
											<div class='spinner-border ml-5' role='status'>
												<span class='visually-hidden' hidden>
													Loading...
												</span>
											</div>
										</div>
									)}
									<h6>Get Instagram Business Data </h6>
									<FacebookLogin
										appId='149964243732146'
										autoLoad={false}
										textButton='Get Facebook Pages'
										size='small'
										fields='name,email,picture'
										scope='business_management,instagram_manage_insights'
										callback={(e) => {
											localStorage.setItem('fblst_149964243732146', e.accessToken);
											setFbsToken(e.accessToken);
											dispatch(getInstagramPage(e.accessToken));
										}}
									/>
								</div>
							)}
						</div>
					</div>
				</div>
			</div>

			<div className='container-fluid px-4 mt-4'>
				<div className='row align-items-stretch'>
					<div className='col-xl-9 col-lg-8 col-md-12 d-flex align-items-stretch justify-content-strech'>
						<div className='card w-100'>
							<div className='card-body'>
								<h4 className='card-title'>Impressions Statastics</h4>
								<div>
									<Line data={data} options={options} width='400' />
								</div>
							</div>
						</div>
					</div>
					<div className='col-xl-3 col-lg-4 col-md-12 d-flex align-items-stretch justify-content-strech'>
						<div className='card w-100'>
							<div className='card-body'>
								<h4 className='card-title'>Instagram Account Stats</h4>
								<table className='table browser mt-4 no-border'>
									<tbody>
										{userInsights.data ? (
											userInsights.data.map((el, idx) => {
												return (
													<tr key={idx}>
														<td>{el.name}</td>
														<td align='right'>
															<span>{el.values[0].value}</span>
														</td>
														<td align='right'>
															<span>
																{new Date(el.values[0].end_time)
																	.toISOString()
																	.slice(0, 10)}
															</span>
														</td>
													</tr>
												);
											})
										) : (
											<tr>
												<td>Please wait ...</td>
											</tr>
										)}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div className='row align-items-stretch mt-4'>
					<div className='col-md-12 d-flex align-items-stretch justify-content-center'>
						<div className='card w-100'>
							<div className='card-body'>
								<h4 className='card-title'>Media Stats</h4>
								<div className='row'>
									{media ? (
										media.map((el, idx) => {
											return (
												<div className='col-md-3' key={idx}>
													<div className='card'>
														{el.media_type === 'VIDEO' ? (
															<video src={el.media_url} alt='media' height={200} />
														) : (
															<img
																src={el.media_url}
																alt='media'
																height={200}
																style={{ objectPosition: 'center', objectFit: 'cover' }}
															/>
														)}
														<div className='card-body'>
															<p
																className='card-text'
																style={{
																	minHeight: 200,
																	maxHeight: 200,
																	overflow: 'auto',
																}}>
																{el.caption}
															</p>
														</div>
														<div className='card-footer d-flex flex-wrap'>
															{el.insights.data?.map((insight, index) => {
																return (
																	<div key={index} className='mr-1'>
																		<p>
																			{insight.title} :
																			<span>
																				<b>{insight.values[0].value}</b>
																			</span>
																		</p>
																	</div>
																);
															})}
														</div>
													</div>
												</div>
											);
										})
									) : (
										<h3>Please wait</h3>
									)}
									<div className='col-12 text-center'>
										<button className='btn btn-dark btn-lg px-5' onClick={getMoreMedia}>
											Load More
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
