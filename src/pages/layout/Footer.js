import React from 'react';

const getYear = () => {
	const date = new Date();
	return date.getFullYear();
};

export default function Footer() {
	return (
		<>
			<footer className='footer text-center text-muted'>
				© {getYear()} Dashboard Admin by <a href='https://www.clozette.com/'>clozette</a>{' '}
			</footer>
		</>
	);
}
