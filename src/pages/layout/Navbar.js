import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
// import * as Icon from 'react-feather';

import logo from '../../assets/images/logo.png';

export default function Navbar() {
	const { userSelected } = useSelector((state) => state.AuthReducer);

	return (
		<header className='topbar' data-navbarbg='skin1'>
			<nav className='navbar top-navbar navbar-expand-md navbar-dark'>
				<div className='navbar-header'>
					<div
						className='nav-toggler waves-effect waves-light d-block d-md-none'
						aria-controls='#logo'
						data-target='#logo'
						// aria-aria-expanded='false'
					>
						<i className='ti-menu ti-close'></i>
					</div>
					<div
						className='topbartoggler d-block d-md-none waves-effect waves-light'
						data-toggle='collapse'
						data-target='#navbarSupportedContent'
						aria-controls='navbarSupportedContent'
						aria-expanded='false'
						aria-label='Toggle navigation'>
						<i className='ti-more'></i>
					</div>
				</div>
				<div id='logo' className='navbar-brand text-center' style={{ padding: '0px 10px 0px 10px' }}>
					<Link to='/'>
						<img src={logo} alt='logo-clozette' width={40} />
					</Link>
				</div>
				<div className='navbar-collapse collapse ml-0' id='navbarSupportedContent' data-navbarbg='skin1'>
					<ul className='navbar-nav w-100 align-items-center' style={{ height: 90 }}>
						<li className='nav-item ml-0 ml-md-3 ml-lg-0'>
							<h2 className='text-white'>Dashboard {userSelected.name || ''} </h2>
						</li>
						<li className='nav-item ml-auto'>
							<div className='upgrade-btn'></div>
						</li>
					</ul>
				</div>
			</nav>
		</header>
	);
}
