import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import * as Icon from 'react-feather';
import { useDispatch, useSelector } from 'react-redux';
import { getLogout } from '../../redux/actions/userAction';

export default function Sidebar() {
	const dispatch = useDispatch();
	const handleLogout = (e) => {
		localStorage.clear();
		window.location.reload(true);
		dispatch(getLogout());
	};
	const { userSelected } = useSelector((state) => state.AuthReducer);
	return (
		<>
			<aside className='left-sidebar' data-sidebarbg='skin6'>
				<div className='user-profile text-center pt-4'>
					<div className='d-flex align-items-center justify-content-center pb-3'>
						<div className='dropdown sub-dropdown'>
							{/* <button
								id='dropdownMenuButton'
								type='button'
								className='btn profile-pic rounded-circle position-relative'
								data-toggle='dropdown'
								aria-haspopup='true'
								aria-expanded='false'></button> */}
							<div className='profile-section pt-3'>
								<p className='font-weight-light mb-0 font-18'>{userSelected.name}</p>
								<span className='op-7 font-14'>{userSelected.name}</span>
							</div>
						</div>
					</div>
				</div>

				<div className='scroll-sidebar h-100' data-sidebarbg='skin6'>
					<nav className='sidebar-nav d-flex h-100'>
						<ul id='sidebarnav h-100'>
							<li className='nav-small-cap'>
								<span className='hide-menu'>Pages</span>
							</li>
							<li className='sidebar-item'>
								<NavLink
									to='/home'
									activeClassName='active'
									activeStyle={{ color: 'red' }}
									className='sidebar-link sidebar-link'
									href='/'
									aria-expanded='false'>
									<Icon.Home />
									<span className='hide-menu ml-2'>Dashboard</span>
								</NavLink>
							</li>
							{/* <li className='sidebar-item'>
								<NavLink
									to='/user'
									activeClassName='active'
									activeStyle={{ color: 'red' }}
									className='sidebar-link sidebar-link'
									href='/user'
									aria-expanded='false'>
									<Icon.User />
									<span className='hide-menu ml-2'>Profile</span>
								</NavLink>
							</li> */}
							<hr />
							<li className='sidebar-item'>
								<Link to='/login' className='d-lg-block ml-2'>
									<div onClick={handleLogout} className='btn btn-block btn-danger d-flex'>
										<Icon.LogOut />
										<span className='hide-menu ml-2'>Log Out</span>
									</div>
								</Link>
							</li>
						</ul>
					</nav>
				</div>
			</aside>
		</>
	);
}
