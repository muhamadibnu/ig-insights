import React from 'react';

import Navbar from './Navbar';
import Sidebar from './Sidebar';
import Footer from './Footer';

export default function Layout({ children }) {
	return (
		<>
			<div
				id='main-wrapper'
				data-theme='dark'
				data-layout='vertical'
				data-navbarbg='skin1'
				data-sidebartype='mini-sidebar'
				data-sidebar-position='fixed'
				data-header-position='fixed'
				data-boxed-layout='full'>
				<div className='app-container' data-navbarbg='skin1'></div>
				<Navbar />
				<Sidebar />
				<div className='page-wrapper'>
					{children}
					<Footer />
				</div>
			</div>
		</>
	);
}
