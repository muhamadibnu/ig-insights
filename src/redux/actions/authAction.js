// import { push } from 'connected-react-router';
// import { useHistory, Redirect } from 'react-router';
// import { baseUrl } from '../../config/baseUrl';
import Axios from '../../helpers/Axios';
// import axios from 'axios'
import Swal from 'sweetalert2';

export const getLogin = (fullName, email, password, history) => {
	return async (dispatch) => {
		try {
			const url = 'https://arcane-tundra-63156.herokuapp.com/users/login';
			// dispatch({ type: 'SET_OPEN_LODING' });
			const data = { fullName, email, password };
			const response = await Axios.Post({
				url,
				data,
			});
			if (response) {
				localStorage.setItem('accessToken', response.access_token);
				localStorage.setItem('email', response.email);
				localStorage.setItem('id', response.id);
				history.push('/');
			}

			// if (response.code === 'OK') {
			// 	const { accessToken, refreshToken } = response.data;
			// 	localStorage.setItem('accessToken', accessToken);
			// 	localStorage.setItem('refreshToken', refreshToken);
			// 	// push('/');
			// 	dispatch({
			// 		type: 'SET_USER',
			// 		payload: response.data,
			// 	});
			// }

			// if (response.status === 'OK') {
			// 	sessionStorage.setItem('session_id', response['session_id']);
			// 	sessionStorage.setItem('uid', response.uid);
			// 	sessionStorage.setItem('user_name', response['user_name']);
			// 	const profile = await Axios.Post({
			// 		url: baseUrl + '/api/getProfile',
			// 		data: {
			// 			uid: response.uid,
			// 		},
			// 	});

			// 	if (profile) dispatch({ type: 'SET_CLOSE_LODING' });

			// 	dispatch({
			// 		type: 'SET_USER_PROFILE',
			// 		payload: profile.profile,
			// 	});
			// } else if (response.status === 'ERROR') {
			// 	dispatch({ type: 'SET_CLOSE_LODING' });
			// 	dispatch({
			// 		type: 'SET_OPEN_SNACKBAR',
			// 		payload: {
			// 			status: true,
			// 			severity: 'error',
			// 			message: response.message,
			// 		},
			// 	});
			// }

			// dispatch({
			// 	type: 'SET_USER',
			// 	payload: response,
			// });

			// dispatch({
			// 	type: 'SET_RESET_PASSWORD',
			// 	payload: response,
			// });
		} catch (error) {
			console.log(error.response);
			if (error.response) {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: error.response.data.message,
				});
			}
			// dispatch({ type: 'SET_CLOSE_LODING' });
			// dispatch({
			// 	type: 'SET_OPEN_SNACKBAR',
			// 	payload: {
			// 		status: true,
			// 		severity: 'error',
			// 		message: error,
			// 	},
			// });
		}
	};
};
