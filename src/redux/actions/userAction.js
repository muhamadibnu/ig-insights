import axios from 'axios';
import { apiUrl } from '../../config/baseUrl';
import Swal from 'sweetalert2';

export const getUserInsightsByDay = (igToken) => {
	return async (dispatch) => {
		try {
			const options = {
				method: 'GET',
				url: `${apiUrl}/users/${igToken}/insights?metric=reach,impressions,website_clicks&period=day&since&until`,
				headers: {
					access_token: localStorage.getItem('fblst_149964243732146'),
				},
			};
			const response = await axios.request(options);
			const { data } = response;
			dispatch({
				type: 'SET_USER_INSIGHTS',
				payload: data,
			});
		} catch (error) {
			console.log(error);
		}
	};
};

export const patchDashboardSelected = (igToken) => {
	return async (dispatch) => {
		try {
			dispatch({
				type: 'SET_DASHBOARD_SELECTED',
				payload: igToken,
			});
		} catch (error) {
			console.log(error);
		}
	};
};

export const fetchMedia = (instagramId, urlPage = '') => {
	return async (dispatch) => {
		try {
			const access_token = localStorage.getItem('fblst_149964243732146');
			let urlInst = `${apiUrl}/instagram`;
			if (urlPage) {
				urlInst = urlPage;
			}
			const responses = await axios.request({
				method: 'GET',
				url: urlInst,
				headers: {
					access_token,
					instagramId,
				},
			});
			const { data } = responses;
			dispatch({
				type: 'SET_USER_MEDIA',
				payload: { data: data.data, urlPage: data.paging },
			});
		} catch (error) {
			console.log(error);
			dispatch({
				type: 'IS_ERROR',
				payload: error.name,
			});
		} finally {
			dispatch({
				type: 'IS_LOADING',
				payload: false,
			});
		}
	};
};

export const getInstagramPage = (facebookAcessToken) => {
	return async (dispatch) => {
		try {
			dispatch({ type: 'SET_OPEN_LODING' });
			const response = await axios({
				method: 'GET',
				url: `${apiUrl}/instagram/get-id`,
				headers: {
					access_token: facebookAcessToken,
				},
			});

			let { data: facebookPages } = response;

			const getIgBussinesId = async (facebookPageId) => {
				const response = await axios({
					method: 'GET',
					url: `${apiUrl}/instagram/get-id/redirect`,
					params: { facebookPageId },
					headers: {
						access_token: localStorage.getItem('fblst_149964243732146'),
					},
				});
				const { data } = response;
				return data.instagram_business_account ? data.instagram_business_account.id : '';
			};

			facebookPages.map(async (el) => {
				el.instagram_business_id = await getIgBussinesId(el.id);
				return el;
			});

			if (facebookPages.length === 0) {
				dispatch({ type: 'SET_OPEN_LODING' });
				Swal.fire({
					icon: 'info',
					title: 'Instrucktion Creat Facebook Page',
					titleText: 'Instrucktion Creat Facebook Page',
					html: '1. Openg Link <a href="https://www.facebook.com/pages/create" target="_blank">Facebook Pages Create</a> <br/> 2. Fill page name and category field And then click <b>Save</b> button <br/> 3. Go to the sidebar and scroll down to <b>Settings</b> tab and click it <br/> 4. Scroll down to <b>Instagram</b> tab and click connect account make sure your Instagram is isntagram business account <br/> 5. Logout dashboard and Login again',
				});
			} else {
				dispatch({ type: 'SET_CLOSE_LODING' });
				dispatch({
					type: 'SET_FACEBOOK_PAGES',
					payload: facebookPages,
				});
			}
		} catch (error) {
			console.log(error);
		}
	};
};

export const fetchUserInsights28Days = () => {
	return async (dispatch) => {
		let until = new Date().toISOString().slice(0, 10);
		let since = new Date();
		let sinceDate = since.setDate(since.getDate() - 28);
		// console.log(new Date(sinceDate).toISOString().slice(0, 10), until, '<<<< allo')
		try {
			const { data } = await axios.request({
				method: 'GET',
				url: 'https://arcane-tundra-63156.herokuapp.com/users/17841401487529790/insights',
				params: {
					metric: 'impressions,reach',
					period: 'days_28',
					since: new Date(sinceDate).toISOString().slice(0, 10),
					until,
				},
				headers: {
					access_token: localStorage.getItem('fblst_149964243732146'),
				},
			});
			dispatch({
				type: 'FETCH_USER_IMPRESSIONS_AND_REACH',
				payload: data.data,
			});
			console.log(data.data, '<<<< data insights');
		} catch (error) {
			console.log(error);
		}
	};
};

export const getLogout = () => {
	return (dispatch) => {
		dispatch({
			type: 'CLEAR_STORE',
			payload: '',
		});
	};
};
