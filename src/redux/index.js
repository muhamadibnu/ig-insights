import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { connectRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';

//reducer
import { AuthReducer } from './reducer/authReducer';
// add reducer here

const reducer = (history) =>
	combineReducers({
		router: connectRouter(history),
		AuthReducer,
		// add reducer here
	});

export const history = createBrowserHistory();
export default function configureStore(preloadedState) {
	const store = createStore(
		reducer(history),
		preloadedState,
		compose(
			applyMiddleware(routerMiddleware(history), thunk)
			// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
		)
	);
	return store;
}
