const intialStore = {
	userSelected: '',
	userProfile: {},
	statusPremium: {},
	userInsights: [],
	media: [],
	urlPage: '',
	pagePermission: [],
	impressionsAndReach: [],
	loading: false,
};

export const AuthReducer = (state = intialStore, { type, payload }) => {
	switch (type) {
		case 'SET_DASHBOARD_SELECTED':
			return {
				...state,
				userSelected: payload,
			};
		case 'SET_USER_PROFILE':
			return {
				...state,
				userProfile: payload,
			};
		case 'SET_CHECK_PREMIUM':
			return {
				...state,
				statusPremium: payload,
			};
		case 'SET_USER_INSIGHTS':
			return {
				...state,
				userInsights: payload,
			};
		case 'SET_USER_MEDIA':
			return {
				...state,
				media: [...state.media, ...payload.data],
				urlPage: payload.urlPage.next,
			};
		case 'SET_FACEBOOK_PAGES':
			console.log('payload: ', payload);
			return {
				...state,
				pagePermission: payload,
			};
		case 'FETCH_USER_IMPRESSIONS_AND_REACH':
			return {
				...state,
				impressionsAndReach: payload,
			};
		case 'CLEAR_STORE':
			return {
				...state,
				userSelected: '',
				userProfile: {},
				statusPremium: {},
				userInsights: [],
				media: [],
				urlPage: '',
				pagePermission: [],
				impressionsAndReach: [],
			};
		case 'SET_OPEN_LODING':
			return {
				...state,
				loading: true,
			};
		case 'SET_CLOSE_LODING':
			return {
				...state,
				loading: false,
			};
		default:
			return state;
	}
};
