import { Home } from '../pages/Home';
import { Login } from '../pages/Auth/Login';

export const routes = [
	{
		path: '/login',
		component: Login,
		exact: true,
		strict: true,
		isPublic: true,
		type: 'auth',
	},
	{
		path: '/home',
		component: Home,
		exact: true,
		strict: true,
		isPublic: false,
		type: 'dashboard',
	},
];
